#!/usr/bin/env bash
# Set DOCKERBUILD_IMAGE_VER=<version number> to skip the version query
# when this script is processed. For example:
#
#     $ DOCKERBUILD_IMAGE_VER=20.04 ./run-bash.sh
#

# The name of the source image
source_image_name="ubuntu-${ubuntu_core_image_base_version:?}"

# The name we should use for the new image
ubuntu_core_image_name="ubuntu-${ubuntu_core_image_base_version:?}-qed"

# Hostname inside our container
ubuntu_core_image_hostname="ubuntu_docker"

ubuntu_core_container_name_bash="${ubuntu_core_image_name:?}"

ubuntu_core_docker_snapshot_basename="dockerimg_${ubuntu_core_image_name:?}-qed"

# default username in the container
ubuntu_core_username=dockeruser

#
#  Add any extra files over to the bootstrap directory
#
#message_banner "Add Extra Files to Bootstrap Directory"

#execute_command "cp ${HOME}/.gitconfig bootstrap/homedir/."
#execute_command_checked "mkdir -p -m 0700 bootstrap/homedir/.ssh"
#execute_command "cp ${HOME}/.ssh/config bootstrap/homedir/.ssh/."
#execute_command "cp ${HOME}/.ssh/id_rsa_qed* bootstrap/homedir/.ssh/."


