#!/usr/bin/env bash
source ../scripts/loadenv.bashrc
source ../params-common.bash


# Load build-specific params file, if it exists.
load_params "."


options_run=(
    -h ${ubuntu_core_image_hostname:?}
    --name="${ubuntu_core_container_name_bash:?}"
    -it ${ubuntu_core_image_name:?} /bin/bash
)


message_std_nocrlf "${yellow}"
message_banner "Launch Image ${ubuntu_core_image_name:?}"
message_std_nocrlf "${normal}"

execute_command "docker run ${options_run[*]}"
session_err=$?


# print out stats about how long this script executed
message_std_nocrlf "${cyan}"
times
message_std_nocrlf "${normal}"


# Get the container ID
container_id=$(get_container_id ${ubuntu_core_container_name_bash:?})
if [ ! -z "${container_id}" ]; then
    message_std "${ice}-- Container ID: ${yellow}${container_id}${normal}"
fi


if [ $session_err -ne 0 ]; then
    message_std "${ice}-- ${red}Docker returned an error when running the session.${normal}"
    save_docker_container_to_image ${container_id:?} ${ubuntu_core_image_name:?} N
else
    # Save the docker container to an image.
    save_docker_container_to_image ${container_id:?} ${ubuntu_core_image_name:?} Y
fi


# Remove the container
if [ ! -z "${container_id}" ]; then
    message_std "${ice}-- Removing Container ID: ${yellow}${container_id:?}${normal}"
    docker container rm ${container_id:?} > /dev/null
fi


# Save the container as a .tgz (optional)
export_docker_image_to_tarball ${ubuntu_core_image_name:?} ${ubuntu_core_docker_snapshot_basename:?}


# Pass on the error code from the docker command
exit $session_err

