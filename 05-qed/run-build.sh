#!/usr/bin/env bash
source ../scripts/loadenv.bashrc
source ../params-common.bash


# Load build-specific params file, if it exists.
load_params "."


message_std_nocrlf "${yellow}"
message_banner_2lines "Build Docker Image" "${ubuntu_core_image_name}"
message_std_nocrlf "${normal}"


# Parameter Defaults
param_dry_run=0
param_dockersnip_files=()
param_build_args=()
param_use_copy_paths=0
param_copy_path_list=()


# Print out usage informationf or the application.
function usage()
{
    message_std "usage: ${cyan}$0 [options]${normal}"
    message_std ""
    message_std "options:"
    message_std "  ${cyan}--add-path-to-homedir=<path>${normal}  : Add a local path to be added to the \$HOME dir in the image.a"1
    message_std "  ${cyan}--append-dockersnip=<filename>${normal}: Append docker snippet to end of Dockerfile"
    message_std "  ${cyan}--build-arg=\"<argument>\"${normal}      : Add a '--build-arg' to the docker build command"
    message_std "  ${cyan}--dry-run${normal}                     : Generate the Dockerfile and print docker build command but don't run it"
    message_std "  ${cyan}-h, --help${normal}                    : Display this help message and exit."
    message_std ""
    message_std "You can bypass the ${cyan}version${normal} question by setting the envvar"
    message_std "${cyan}DOCKERBUILD_IMAGE_VER${normal} to the version you want to run. For example:"
    message_std ""
    message_std "    \$${magenta}DOCKERBUILD_IMAGE_VER=18.04 ./run-build.bash${normal}"
    message_std ""
    message_std "build args:"
    message_std "  ${cyan}--build-arg=\"GITLAB_RUNNER_TOKEN=<token>\"${normal}     : Your gitlab-runner token (required)"
    message_std "  ${cyan}--build-arg=\"GITLAB_RUNNER_USERNAME=username\"${normal} : default='gitlab-runner'"
    message_std ""
}



# Parse command line arguments
for i in "$@"; do
    case $i in
        -h|--help)
            usage
            exit 1
            ;;
        --dry-run)
            param_dry_run=1
            ;;
        --add-path-to-homedir=*)
            param_use_copy_paths=1
            param_copy_path_list+=("${i#*=}")
            shift
            ;;
        --append-dockersnip=*)
            param_dockersnip_files+=("${i#*=}")
            shift
            ;;
        --build-arg=*)
            param_build_args+=("${i#*=}")
            shift
            ;;
        *)
            message_std "[WARNING] Unknown provided: $i"
            usage
            shift
            ;;
    esac
done

if [ -e bootstrap.bash ]; then
    source bootstrap.bash
fi

# Copy the bootstrap files over
copy_bootstrap_files  bootstrap/homedir param_copy_path_list


# Generate the Dockerfile
message_std "${yellow}Build using base image ${magenta}ubuntu:${ubuntu_core_image_base_version:?}${normal}"
if [ -e Dockerfile ]; then
    rm Dockerfile
fi
#sed "s/ubuntu:latest/ubuntu:${ubuntu_core_image_base_version:?}/" Dockerfile-template > Dockerfile
if [ ! -e Dockerfile ]; then
    cp Dockerfile-template Dockerfile
fi


# Append the snippets to the end of the Dockerfile (if any)
append_dockersnip_files param_dockersnip_files

# Set Docker build parameters
build_params=(
    --force-rm
    -t ${ubuntu_core_image_name:?}
    --network=host
    #--build-arg IMAGE_VERSION=${ubuntu_core_image_base_version:?}
    --build-arg SOURCE_IMAGE=${source_image_name:?}
    --build-arg SYSTEM_LABEL=${ubuntu_core_image_name}
    #--build-arg USERNAME=dockeruser
    --build-arg USERNAME=${ubuntu_core_username}
    --build-arg CMAKE_VER=3.26.4
)


# Add in build args (if any)
# TODO: Clean up and move over to a function?
if [ ${#param_build_args[@]} -gt 0 ]; then
    for iarg in ${param_build_args[*]}; do
        echo "--build-arg ${iarg}"
        build_params+=("--build-arg ${iarg}")
    done
fi


# Grab proxy envvars if they exist
env_proxies=()
if [ ! -z ${http_proxy} ]; then
    env_proxies+=(--build-arg http_proxy=${http_proxy})
fi
if [ ! -z ${https_proxy} ]; then
    env_proxies+=(--build-arg https_proxy=${https_proxy})
fi
if [ ! -z ${ftp_proxy} ]; then
    env_proxies+=(--build-arg ftp_proxy=${ftp_proxy})
fi
if [ ! -z ${ftps_proxy} ]; then
    env_proxies+=(--build-arg ftps_proxy=${ftps_proxy})
fi
if [ ! -z ${no_proxy} ]; then
    env_proxies+=(--build-arg no_proxy=${no_proxy})
fi
if [ ! -z ${SSL_CERT_FILE} ]; then
    env_proxies+=(--build-arg SSL_CERT_FILE=${SSL_CERT_FILE})
fi
if [ ! -z ${REQUESTS_CA_BUNDLE} ]; then
    env_proxies+=(--build-arg REQUESTS_CA_BUNDLE=${REQUESTS_CA_BUNDLE})
fi
build_params+=(${env_proxies[@]})



# Build the Docker image
message_std "\$ ${magenta}docker build ${build_params[*]} .${normal}"

execute_command "docker image rm ${ubuntu_core_image_name:?}"

err=0
if [ ${param_dry_run} -eq 0 ]; then
    docker build ${build_params[*]} .
    err=$?
else
    echo -e "Skip ${cyan}build${normal} due to ${red}--dry-run${normal} flag."
fi


# Print out status of the build
echo ""
if [ ${err:?} -ne 0 ]; then
    message_failure
elif [ ${param_dry_run} -eq 0 ]; then
    message_success
    execute_command "rm Dockerfile"

    # TODO: This needs to be made generic / parameterized
    if [[ "$(find_in_array "Dockersnip-BUILDER" param_dockersnip_files)" == "1" ]]; then
        tmpstr=$(random_string_alnum 8)
        container_tmp="${ubuntu_core_image_name:?}${tmpstr:?}"
        filename_tmp="MyProj-$(date +'%Y%m%d')-Linux-x86_64.zip"
        message_std "image name: ${ubuntu_core_image_name}"
        execute_command "docker create -ti --name ${container_tmp:?} ${ubuntu_core_image_name:?} bash"
        execute_command "docker cp ${container_tmp:?}:/home/dockeruser/dev/MyProj/distro ."
        execute_command "docker rm -f ${container_tmp:?}"
    fi
else
    echo -e "Skip ${cyan}dockerfile removal${normal} due to ${red}--dry-run${normal} flag."
fi
echo ""


# Clean up files copied into bootstrap directory
if [ ${param_dry_run} -eq 0 ]; then
    cleanup_bootstrap_files bootstrap/homedir param_copy_path_list
else
    echo -e "Skip ${cyan}cleanup${normal} due to ${red}--dry-run${normal} flag."
fi


# print out stats about how long this script executed
message_std_nocrlf "${cyan}"
times
message_std_nocrlf "${normal}"


exit $err

