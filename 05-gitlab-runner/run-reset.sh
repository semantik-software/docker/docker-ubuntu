#!/usr/bin/env bash
source ../scripts/loadenv.bashrc
source ../params-common.bash


# Load build-specific params file, if it exists.
load_params "."


message_std_nocrlf "${yellow}"
message_banner "Remove Image ${ubuntu_core_image_name:?}"
message_std_nocrlf "${normal}"

execute_command "docker image rm ${ubuntu_core_image_name}"
execute_command "docker builder prune --force"

# print out stats about how long this script executed
message_std_nocrlf "${cyan}"
times
message_std_nocrlf "${normal}"

