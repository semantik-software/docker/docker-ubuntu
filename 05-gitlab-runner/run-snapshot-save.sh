#!/usr/bin/env bash
source ../scripts/loadenv.bashrc
source ../params-common.bash


# Load build-specific params file, if it exists.
load_params "."


message_std_nocrlf "${yellow}"
message_banner_2lines "Save Image" "${ubuntu_core_image_name:?} -> ${ubuntu_core_docker_snapshot_basename:?}.tgz"
message_std_nocrlf "${normal}"

execute_command "docker image save ${ubuntu_core_image_name:?} -o ${ubuntu_core_docker_snapshot_basename:?}.tar" && \
execute_command "gzip ${ubuntu_core_docker_snapshot_basename:?}.tar" && \
execute_command "mv ${ubuntu_core_docker_snapshot_basename:?}.tar.gz ${ubuntu_core_docker_snapshot_basename:?}.tgz"

err=$?


# print out stats about how long this script executed
message_std_nocrlf "${cyan}"
times
message_std_nocrlf "${normal}"


exit ${err:?}

