#!/usr/bin/env bash
# Set DOCKERBUILD_IMAGE_VER=<version number> to skip the version query
# when this script is processed. For example:
#
#     $ DOCKERBUILD_IMAGE_VER=20.04 ./run-bash.sh
#

if [ -z ${DOCKER_HELPER_LOADENV} ]; then
    source scripts/loadenv.bashrc
fi

version_list=(
    23.04
    22.04
    20.04
    18.04
)

ubuntu_core_image_base_version=${version_list[0]}

SELECTION_OVERRIDE=${DOCKERBUILD_IMAGE_VER}
message_std ""
message_std "${yellow}Please select the version to use${normal}:"
message_std_nocrlf "${cyan}"
ubuntu_core_image_base_version=$(select_with_default "${version_list[@]}")
message_std_nocrlf "${normal}"
message_std "Using image version: ${magenta}${ubuntu_core_image_base_version}${normal}"
message_std ""
unset SELECTION_OVERRIDE


