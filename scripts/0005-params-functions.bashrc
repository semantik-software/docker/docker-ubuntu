#!/usr/bin/env bash
scriptpath=$(dirname  ${BASH_SOURCE:?})
scriptname=$(basename ${BASH_SOURCE:?})
loadenv_notify ${scriptname}


# load_params()
# - Loads params file
# param1: relative directory to cwd
load_params() {
    local filepath="${1:-.}"
    if [ -f "${filepath}/params.bash" ]; then
        message_std "${yellow}Loading '${magenta}${filepath}/params.bash${yellow}'.${normal}"
        source ${filepath}/params.bash
    fi
}


