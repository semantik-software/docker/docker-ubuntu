#!/usr/bin/env bash

# ---------------------------------
#   C O L O R   U T I L I T I E S
# ---------------------------------

unset bold
unset underline
unset standout
unset standout_end
unset normal
unset dim
unset black
unset red
unset green
unset yellow
unset blue
unset magenta
unset cyan
unset ice
unset white


if [[ "${TERM}" == "dumb" ]]; then

    echo "" > /dev/null

else

    # check if stdout is a terminal...
    if test -t 1; then
        # see if it supports colors...
        ncolors=$(tput colors)

        if test -n "$ncolors" && test $ncolors -ge 8; then
            export bold="$(tput bold)"
            export underline="$(tput smul)"
            export standout="$(tput smso)"
            export standout_end="$(tput rmso)"
            export normal="$(tput sgr0)"
            export dim="$(tput dim)"
            export black="$(tput setaf 0)"
            export red="$(tput setaf 1)"
            export green="$(tput setaf 2)"
            export yellow="$(tput setaf 3)"
            export blue="$(tput setaf 4)"
            export magenta="$(tput setaf 5)"
            export cyan="$(tput setaf 6)"
            export ice="$(tput setaf 43)"
            export white="$(tput setaf 7)"
        fi
    fi
fi


#function color() {
#    local _color=${1:?}
#    local _numcolors=`tput colors`
#    if [ ${_color} -lt ${_numcolors} ]; then
#        printf "$(tput setaf ${_color})"
#    fi
#}

# Set a color by color number.
# Usage: echo -e "$(color 4)mytext"
# param1: the color number (int)
function color() {
    local _color=${1:?}
    local _numcolors=`tput colors`
    if [ -n ${_numcolors} ] && [ ${_numcolors} -gt ${_color} ]; then
        printf "$(tput setaf ${_color})"
    fi
}



# Print out the color table
function print_color_table() {
    local num_colors=`tput colors`
    echo "num_colors=${num_colors}" >&2
    for ((i=0; i<${num_colors}; i++)); do
        local color=$(tput setaf ${i})
        printf "${color}%4s${normal}" $i
        if [[ "$(( ($i+1) % 20))" == "0" ]]; then
            printf "\n"
        fi
    done
}



