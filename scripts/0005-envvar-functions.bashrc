#!/usr/bin/env bash
scriptpath=$(dirname  ${BASH_SOURCE:?})
scriptname=$(basename ${BASH_SOURCE:?})
loadenv_notify ${scriptname}


# envvar_append_or_create
#  $1 = envvar name
#  $2 = string to append
function envvar_append_or_create() {
    # envvar $1 is not set
    if [[ ! -n "${!1+1}" ]]; then
        export ${1}="${2}"
    else
        export ${1}="${!1}:${2}"
    fi
}


# envvar_prepend_or_create
#  $1 = envvar name
#  $2 = string to prepend
function envvar_prepend_or_create() {
    # envvar $1 is not set
    if [[ ! -n "${!1+1}" ]]; then
        export ${1}="${2}"
    else
        export ${1}="${2}:${!1}"
    fi
}


# envvar_set_or_create
#  $1 = envvar name
#  $2 = string to prepend
function envvar_set_or_create() {
    export ${1}="${2}"
}


# envvar_remove_path_entry
# $1 = A path style envvar name
# $2 = Entry to remove from the path.
function envvar_remove_path_entry() {
    local envvar=${1:?}
    local to_remove=${2:?}
    local new_value=${!envvar}
    if [ ! -z ${envvar} ]; then
        new_value=:${new_value}:
        new_value=${new_value//:${to_remove}:/:}
        new_value=${new_value#:1}
        new_value=${new_value%:1}
        export ${envvar}=${new_value}
    fi
}


