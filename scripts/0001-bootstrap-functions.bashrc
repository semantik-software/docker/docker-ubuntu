#!/usr/bin/env bash
scriptpath=$(dirname  ${BASH_SOURCE:?})
scriptname=$(basename ${BASH_SOURCE:?})

color_loadmsg=${green}
color_version=${green}
color_notify=${ice}
color_warning=${red}
color_envvar=${purple}
color_value=${red}


bashrc_notify() {
    printmsg=0
    if [ ! -n "$SSH_CLIENT" ] && [ ! -n "$SSH_TTY" ]; then
        printmsg=1
    fi
    if [[ "$printmsg" == "1" ]]; then
        scriptname=$(basename ${BASH_SOURCE:?})
        echo -e "$1"
    fi
}


loadenv_notify() {
    local _msg=${1:?}
    bashrc_notify "${color_loadmsg}LOADENV${normal}: ${color_notify}${_msg:?}${normal}"
}


loadenv_notify ${scriptname}



