#!/usr/bin/env bash
scriptpath=$(dirname  ${BASH_SOURCE:?})
scriptname=$(basename ${BASH_SOURCE:?})
# echo "scriptpath = ${scriptpath}"

shopt -s dotglob
shopt -s nullglob

# TODO: What happens if scriptpath is an empty string?
#       Can it be an empty string using BASH_SOURCE?
FILES=$(find ${scriptpath:?} -name "????-*.bashrc" -print0 | xargs -0 ls)

# debugging:
# echo "${FILES[@]}"

for f in $FILES
do
    source $f
done

export DOCKER_HELPER_LOADENV=1

