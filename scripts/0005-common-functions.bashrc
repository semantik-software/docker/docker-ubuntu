#!/usr/bin/env bash
scriptpath=$(dirname  ${BASH_SOURCE:?})
scriptname=$(basename ${BASH_SOURCE:?})
loadenv_notify ${scriptname}


# -----------------------------------
#   C O M M O N   U T I L I T I E S
# -----------------------------------


# Gets the current script name (full path + filename)
function get_scriptname() {
    # Get the full path to the current script
    local script_name=`basename $0`
    local script_path=$(dirname $(readlink -f $0))
    local script_file="${script_path}/${script_name:?}"
    echo "${script_file}"
}


# Gets the path to the current script (full path)
function get_scriptpath() {
    # Get the full path to the current script
    local script_name=`basename $0`
    local script_path=$(dirname $(readlink -f $0))
    echo "${script_path}"
}


# Get the md5sum of a filename.
# param1: filename
# returns: md5sum of the file.
function get_md5sum() {
    local filename=${1:?}
    local sig=$(md5sum ${filename:?} | cut -d' ' -f1)
    echo "${sig:?}"
}


# Generate a random string
# param1: string_length (1..256)
function random_string_alnum() {
    local len=${1:?}

    if [ $len -gt 256 ]; then
        len=256
    elif [ $len -lt 1 ]; then
        len=1
    fi

    local output=""
    for n in [ 1 2 3 4 5 6 7 8 ]; do
        output="${output}$(echo ${RANDOM} | sha256sum | base64 | cut -d ' ' -f 1 | head -c 32)"
    done
    output=$(echo ${output} | head -c ${len})
    echo -e "${output}"
}



# Custom `select` implementation that allows *empty* input.
# Pass the choices as individual arguments.
# Output is the chosen item, or "", if the user just pressed ENTER.
# Example:
#    choice=$(select_with_default 'one' 'two' 'three')
# Attribution: This is copied with minor modifications from here:
#     https://stackoverflow.com/a/42790579/2059999
#
# If SELECTION_OVERRIDE is set then we use that value, otherwise we
# query the user.
#
select_with_default() {

    local item i=0 numItems=$#

    if [ ! -z ${SELECTION_OVERRIDE} ]; then
        printf %s "${SELECTION_OVERRIDE}"
        return
    fi

    if [[ "${numItems}" == "1" ]]; then
        printf %s "${1}"
        return
    fi

    # Print numbered menu items, based on the arguments passed.
    for item; do         # Short for: for item in "$@"; do
        printf '%s\n' "$((++i))) $item"
    done >&2 # Print to stderr, as `select` does.

    # Prompt the user for the index of the desired item.
    while :; do
        printf %s "${PS3-#? }" >&2 # Print the prompt string to stderr, as `select` does.
        read -r index
        # Make sure that the input is either empty or that a valid index was entered.
        [[ -z $index ]] && break  # empty input
        (( index >= 1 && index <= numItems )) 2>/dev/null || { echo "Invalid selection. Please try again." >&2; continue; }
        break
    done

    if [ -z $index ]; then
        index=1
    fi
    #echo -e ">>> index '${index}'" >&2
    #echo -e ">>> opt1 '${1}'" >&2
    #echo -e ">>> opt2 '${1}'" >&2

    # Output the selected item, if any.
    [[ -n $index ]] && printf %s "${@: index:1}"
}



