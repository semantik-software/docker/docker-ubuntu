#!/usr/bin/env bash
scriptpath=$(dirname  ${BASH_SOURCE:?})
scriptname=$(basename ${BASH_SOURCE:?})
loadenv_notify ${scriptname}



# -----------------------------------
#   D O C K E R   U T I L I T I E S
# -----------------------------------


function append_dockersnip_files()
{
    local arg_dockersnip_files=$1[@]
    local dockersnip_files=("${!arg_dockersnip_files}")

    # Append the snippets to the end of the Dockerfile (if any)
    if [ ${#dockersnip_files[@]} -gt 0 ]; then
        for ifile in ${dockersnip_files[*]}; do
            message_std "${yellow}Append ${cyan}${ifile:?}${yellow} to ${cyan}Dockerfile${normal}"
            execute_command_checked "cat ${ifile:?} >> Dockerfile"
        done
    fi
}


function find_in_array()
{
    local output=0
    local value=${1:?}
    local arg_dockersnip_files=$2[@]
    local dockersnip_files=("${!arg_dockersnip_files}")
    if [[ " ${dockersnip_files[@]} " =~ " ${value} " ]]; then
        output=1
    fi
    echo -e "${output}"
}



#
# Copy a list of paths into some destination
# param1: destination path
# param2: array of paths to copy
#
function copy_bootstrap_files()
{
    local arg_dest_path=${1:?}
    local arg_pathlist=$2[@]
    local pathlist=("${!arg_pathlist}")

    if [ ! -d ${arg_dest_path:?} ]; then
        message_std "${red}[ERROR] Destination path does not exist, or is not a directory.${normal}"
        message_std "${red}        '${arg_dest_path:?}'${normal}"
        exit 27
    fi

    for ipath in "${pathlist[@]}"; do
        message_std_nocrlf "COPY ${cyan}${ipath:?}${normal} to ${cyan}${arg_dest_path:?}${normal}: "
        if [ ! -e ${ipath:?} ]; then
            message_std "${red}FAILED${normal}"
            exit 28
        fi
        cp -pr ${ipath} ${arg_dest_path:?}/.
        err=$?
        if [ $err -ne 0 ]; then
            message_std "${red}FAILED${normal}"
            exit 29
        fi
        message_std "${green}OK${normal}"
    done
}



# Remove a list of paths from a source directory
# param1: source path.
# param2: array of paths to copy
#
function cleanup_bootstrap_files()
{
    local arg_source_path=${1:?}
    local arg_pathlist=$2[@]
    local pathlist=("${!arg_pathlist}")

    if [ ! -d ${arg_source_path:?} ]; then
        message_std "${yellow}[WARNING] Path does not exist, or is not a directory.${normal}"
        message_std "${yellow}          '${arg_dest_path:?}'${normal}"
    else
        # Clean up files copied into bootstrap directory
        for ipath in "${pathlist[@]}"; do
            ipath_basename=$(basename ${ipath:?})
            ipath_remove="${arg_source_path:?}/${ipath_basename:?}"

            message_std_nocrlf "REMOVE ${cyan}${ipath_remove:?}${normal} "
            if [ ! -e ${ipath:?} ]; then
                message_std "${red}FAILED${normal}"
                continue
            fi
            rm -rf ${ipath_remove:?}
            err=$?
            if [ $err -ne 0 ]; then
                message_std "${red}FAILED${normal}"
            fi
            message_std "${green}OK${normal}"
        done
    fi
}


