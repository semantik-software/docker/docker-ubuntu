#!/usr/bin/env bash
scriptpath=$(dirname  ${BASH_SOURCE:?})
scriptname=$(basename ${BASH_SOURCE:?})
loadenv_notify ${scriptname}



# -----------------------------------
#   D O C K E R   U T I L I T I E S
# -----------------------------------


#
# Create a docker volume if one doesn't already exist with the same name.
#
# @param volume_name  The name of the volume to create
#
function docker_create_volume_if_none
{
    local volume_name=${1:?}
    docker volume inspect ${volume_name:?}
    local err=$?
    if [ ${err:?} -ne 0 ]; then
        docker volume create ${volume_name:?}
    fi
    return ${err}
}



#
# Install a docker image from a tarball file
#
# @param image_name  The name of the image. i.e., jenkins/centos:latest
# @param image_file  The filename of the tarball. i.e., ../images/jenkins-centos-latest.tgz
#
function install_docker_image
{
    local image_name=${1:?}
    local image_file_name=${2:?}
    local err=0
    message_std ""
    if [[ "$(docker images -q ${image_name:?} 2> /dev/null)" == "" ]]; then
        message_std "${ice}Docker image ${green}${image_name:?}${ice} not installed...${normal}"
        message_std "${ice}Loading...${normal}"
        message_std ""
        docker load < ${image_file_name:?}
        err=$?
    else
        message_std "${ice}Docker image ${green}${image_name:?}${ice} already installed.${normal}"
    fi
    message_std ""
    return ${err}
}



#
# Get the container id for a particular contianer.
#
# @param container_name The name of the container.
#
function get_container_id() {
    local container_name=${1:?}
    local container_id=$(docker ps -aqf "name=${container_name}")
    echo "${container_id}"
}



#
# param: container_id
# param: image_name
# param: default option (Y)
function save_docker_container_to_image()
{
    local _container_id=${1:?}
    local _image_name=${2:?}
    local _default_option=${3}
    local _err=0

    if [ -z ${_container_id} ]; then
        echo "${red}WARNING${normal}: save_docker_container_to_image():"
        echo "       : 'container_id' is missing or empty."
        return 1
    fi
    if [ -z ${_image_name} ]; then
        echo "${red}WARNING${normal}: save_docker_container_to_image():"
        echo "       : 'image_name' is missing or empty."
        return 1
    fi
    if [[ "${_default_option}" != "Y" && "${_default_option}" != "N" ]]; then
        _default_option="Y"
    fi

    if [[ "${_default_option}" == "N" ]]; then
        message_std ""
        read -r -p "${red}>> ${green}Save session to Docker image?   ${yellow}[y/N]${green} > ${normal}" SAVE_SESSION
        message_std ""
        case "${SAVE_SESSION}" in
            [Yy][Ee][Ss]|[Yy])
                message_std "${ice}-- Session updates WILL be saved.${normal}"

                # Save the container back to the image
                message_std "${ice}-- Save container ${yellow}${_container_id:?}${ice} back to image ${yellow}${_image_name:?}${normal}"
                #docker commit -a ${USER} ${_container_id:?} ${_image_name:?} > /dev/null
                execute_command "docker commit -a ${USER} ${_container_id:?} ${_image_name:?}"
                _err=$?
                ;;
            *)
                message_std "${ice}-- Session updates WILL NOT be saved.${normal}"
                ;;
        esac

    else
        message_std ""
        read -r -p "${red}>> ${green}Save session to Docker image?   ${yellow}[Y/n]${green} > ${normal}" SAVE_SESSION
        message_std ""
        case "${SAVE_SESSION}" in
            [Nn][Oo]|[Nn])
                message_std "${ice}-- Session updates WILL NOT be saved.${normal}"
                ;;
            *)
                message_std "${ice}-- Session updates WILL be saved.${normal}"

                # Save the container back to the image
                message_std "${ice}-- Save container ${yellow}${_container_id:?}${ice} back to image ${yellow}${_image_name:?}${normal}"
                execute_command "docker commit -a ${USER} ${_container_id:?} ${_image_name:?}"
                #docker commit -a ${USER} ${_container_id:?} ${_image_name:?} > /dev/null
                _err=$?
                ;;
        esac
    fi
    return ${_err}
}



# export_docker_image_to_tarball
# param1: image_name
# param2: snapshot_base_filename (i.e., {snapshot_base_filename}.tgz
function export_docker_image_to_tarball()
{
    local _image_name=${1:?}
    local _snapshot_base_filename=${2:?}
    local _err=0

    message_std ""
    read -r -p "${red}>> ${green}Export container to ${_snapshot_base_filename}.tgz?   ${yellow}[y/N]${green} > ${normal}" EXPORT_CONTAINER
    message_std ""

    case "${EXPORT_CONTAINER}" in
        [yY][Ee][Ss]|[Yy])
            message_std "${ice}-- Container will be exported...${normal}"
            execute_command "docker image save ${_image_name:?} -o ${_snapshot_base_filename:?}.tar" && \
            execute_command "gzip ${_snapshot_base_filename:?}.tar" && \
            execute_command "mv ${_snapshot_base_filename:?}.tar.gz ${_snapshot_base_filename:?}.tgz"
            _err=$?
            message_std "${ice}-- Container exported to ${yellow}${_snapshot_base_filename}.tgz${normal}"
            ;;
        *)
            message_std "${ice}-- Container will not be exported.${normal}"
            ;;
    esac
    return ${_err}
}



