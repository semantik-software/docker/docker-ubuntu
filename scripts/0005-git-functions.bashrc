#!/usr/bin/env bash
scriptpath=$(dirname  ${BASH_SOURCE:?})
scriptname=$(basename ${BASH_SOURCE:?})
loadenv_notify ${scriptname}


# get the top-level directory of the current
# repository that the current-working-directory
# is contained in.
bashrc_git_root() {
    local git_root=$(git rev-parse --show-toplevel)
    echo "${git_root:?}"
}


bashrc_git_bashrc_dir() {
    local git_root=$(bashrc_git_root)
    echo "${git_root}/etc/bashrc.d"
}


