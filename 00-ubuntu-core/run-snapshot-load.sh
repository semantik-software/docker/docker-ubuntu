#!/usr/bin/env bash
source ../scripts/loadenv.bashrc
source ../params-common.bash


# Load build-specific params file, if it exists.
load_params "."

printf "${yellow}"
message_banner_2lines "Load Image" "${ubuntu_core_docker_snapshot_basename:?}.tgz -> ${ubuntu_core_image_name:?}"
printf "${normal}"

install_docker_image ${ubuntu_core_image_name:?} ${ubuntu_core_docker_snapshot_basename:?}.tgz

err=$?

# print out stats about how long this script executed
printf "${cyan}"
times
printf "${normal}"

