#!/usr/bin/env bash
source ../scripts/loadenv.bashrc
source ../params-common.bash


# Load build-specific params file, if it exists.
load_params "."


message_std_nocrlf "${yellow}"
message_banner "Launch Image ${ubuntu_core_image_name:?}"
message_std_nocrlf "${normal}"

# Parameters
param_args=()
param_command=""
param_interactive="-i"

# Print out usage informationf or the application.
function usage()
{
    message_std "usage: ${cyan}$0 [-d] [docker run options] [--cmd=<command>]${normal}"
    message_std ""
    message_std "options:"
    message_std "  ${cyan}-d${normal}              : Run in 'daemon' mode (-d) instead of interactive mode (-i)"
    message_std "  ${cyan}[options]${normal}       : Arguments are passed on to docker run"
    message_std "  ${cyan}--cmd=<command>${normal} : Command to run, overriding the default set up in Docker"
    message_std "  ${cyan}-h, --help${normal}      : Display this help message and exit."
    message_std ""
    message_std "You can bypass the ${cyan}version${normal} question by setting the envvar"
    message_std "${cyan}DOCKERBUILD_IMAGE_VER${normal} to the version you want to run. For example:"
    message_std ""
    message_std "    \$${magenta}DOCKERBUILD_IMAGE_VER=18.04 ./run-build.bash${normal}"
    message_std ""
    message_std "${magenta}Example Usage:${normal}"
    message_std ""
    message_std "${yellow}Run the container as a daemon and always restart.${normal}"
    message_std "  \$ ${0} -d --restart always"
    message_std ""
    message_std "${yellow}Start up an interactive bash shell in the container.${normal}"
    message_std "  \$ ${0} --cmd=/bin/bash"
    message_std ""
}


for i in "$@"; do
    case $i in
        -h|--help)
            usage
            exit 1
            ;;
        --cmd=*)
            param_command="${i#*=}"
            shift
            ;;
        -d)
            param_interactive="-d"
            shift
            ;;
        *)
            param_args+=("${i#*=}")
            shift
            ;;
    esac
done


options_run=(
    -h ${ubuntu_core_image_hostname:?}
    --name="${ubuntu_core_container_name_bash:?}"
    #-it ${ubuntu_core_image_name:?}
    ${param_interactive}
)
options_run+=(${param_args[@]})
#options_run+=(${param_interactive})
options_run+=("-t ${ubuntu_core_image_name:?}")
options_run+=(${param_command})

# Launch the container
execute_command "docker run ${options_run[*]}"
session_err=$?


# print out stats about how long this script executed
message_std_nocrlf "${cyan}"
times
message_std_nocrlf "${normal}"


# Get the container ID
container_id=$(get_container_id ${ubuntu_core_container_name_bash:?})
if [ ! -z "${container_id}" ]; then
    message_std "${ice}-- Container ID: ${yellow}${container_id}${normal}"
fi

# If we ran it in daemon mode we should exit here.
if [[ "$param_interactive" == "-d" ]]; then
    exit 0
fi

if [ $session_err -ne 0 ]; then
    message_std "${ice}-- ${red}Docker returned an error when running the session.${normal}"
    save_docker_container_to_image ${container_id:?} ${ubuntu_core_image_name:?} N
else
    # Save the docker container to an image.
    save_docker_container_to_image ${container_id:?} ${ubuntu_core_image_name:?} Y
fi


# Remove the container
if [ ! -z "${container_id}" ]; then
    message_std "${ice}-- Removing Container ID: ${yellow}${container_id:?}${normal}"
    docker container rm ${container_id:?} > /dev/null
fi


# Save the container as a .tgz (optional)
export_docker_image_to_tarball ${ubuntu_core_image_name:?} ${ubuntu_core_docker_snapshot_basename:?}


# Pass on the error code from the docker command
exit $session_err

