#!/usr/bin/env bash
# Set DOCKERBUILD_IMAGE_VER=<version number> to skip the version query
# when this script is processed. For example:
#
#     $ DOCKERBUILD_IMAGE_VER=20.04 ./run-bash.sh
#

# Source image to build our docker image from
source_image_name="ubuntu:${ubuntu_core_image_base_version:?}"

# The name for our new docker image
ubuntu_core_image_name="ubuntu-${ubuntu_core_image_base_version:?}"

# Sets a hostname inside our image
ubuntu_core_image_hostname="ubuntu_docker"

# Default container name when running
ubuntu_core_container_name_bash="${ubuntu_core_image_name:?}"

# Base filename for snapshots
ubuntu_core_docker_snapshot_basename="dockerimg_${ubuntu_core_image_name:?}"

# Username for the default docker user
ubuntu_core_username="dockeruser"


