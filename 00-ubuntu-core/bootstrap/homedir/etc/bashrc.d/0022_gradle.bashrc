#!/usr/bin/env bash
scriptpath=$(dirname  ${BASH_SOURCE:?})
scriptname=$(basename ${BASH_SOURCE:?})

bashrc_notify "     ${color_loadmsg}LOAD${normal}: ${scriptname}"

#---------------------
# Gradle Settings
#---------------------
export GRADLE_HOME=/opt/apps/gradle
bashrc_notify "         :${cyan} Set ${yellow}GRADLE_HOME${cyan} = ${yellow}${GRADLE_HOME:?}${normal}"

export PATH=${PATH:?}:${GRADLE_HOME:?}/bin



