#!/usr/bin/env bash
scriptpath=$(dirname  ${BASH_SOURCE:?})
scriptname=$(basename ${BASH_SOURCE:?})

bashrc_notify "     ${color_loadmsg}LOAD${normal}: ${scriptname}"

#---------------------
# Proxy Settings
#---------------------
myproxy="http://user:nopass@proxy.acme.org:80"

unset SSL_CERT_FILE
unset REQUESTS_CA_BUNDLE

export SSL_CERT_FILE=/opt/certificates/acme_root_ca.cer
export REQUESTS_CA_BUNDLE=${SSL_CERT_FILE:?}

export http_proxy=${myproxy}
export https_proxy=${myproxy}
export ftp_proxy=${myproxy}
export ftps_proxy=${myproxy}

export no_proxy="localhost,127.0.0.1,.acme.org"

