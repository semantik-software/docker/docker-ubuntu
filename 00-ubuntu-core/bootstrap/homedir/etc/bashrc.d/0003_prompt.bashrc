#!/usr/bin/env bash
scriptdir=$(dirname $BASH_SOURCE)
scriptname=$(basename $BASH_SOURCE)


bashrc_notify "     ${color_loadmsg}LOAD${normal}: ${scriptname}"

# ----------------------

# A two-line colored Bash prompt (PS1) with Git branch and a line decoration
# which adjusts automatically to the width of the terminal.
# Recognizes and shows Git, SVN and Fossil branch/revision.
# Screenshot: http://img194.imageshack.us/img194/2154/twolineprompt.png
# Michal Kottman, 2012
#
# Source: https://gist.github.com/mkottman/1936195

RESET="\[\033[0m\]"
GRAY="\[\033[1;30m\]"
RED="\[\033[0;31m\]"
LTRED="\[\033[1;31m\]"
GREEN="\[\033[0;32m\]"
LTGREEN="\[\033[1;32m\]"
BLUE="\[\033[0;34m\]"
LTBLUE="\[\033[1;34m\]"
YELLOW="\[\033[0;33m\]"
PURPLE="\[\033[0;35m\]"
CYAN="\[\033[0;36m\]"
BROWN="\[\033[0;33m\]"

PS_LINE=`printf -- '- %.0s' {1..300}`

function parse_git_branch {
  PS_BRANCH=''
  PS_FILL="${PS_LINE:0:$COLUMNS}"
  if [ -d .svn ]; then
    PS_BRANCH="(svn r$(svn info|awk '/Revision/{print $2}'))"
    return
  elif [ -f _FOSSIL_ -o -f .fslckout ]; then
    PS_BRANCH="(fossil $(fossil status|awk '/tags/{print $2}')) "
    return
  fi
  ref=$(git symbolic-ref HEAD 2> /dev/null) || return
  PS_BRANCH="(git ${ref#refs/heads/}) "
}
PROMPT_COMMAND=parse_git_branch

COLOR_DASHES="${LTBLUE}"
COLOR_GITBRANCH="${PURPLE}"

PS_PRE="${LTBLUE}\j${RESET} "
# PS_PRE="$(expr `dirs -v | wc -l` - 1) ${LTBLUE}\j${RESET} "

#PS_PATH="\w"
#PS_PATH='$(pwd | sed -E -e "s|^$HOME|~|" -e '\''s|^([^/]*/[^/]*/).*(/[^/]*)|\1..\2|'\'')'
PS_PATH='$(pwd|awk -F/ -v "n=$(tput cols)" -v "h=^$HOME" '\''{sub(h,"~");n=0.35*n;b=$1"/"$2} length($0)<=n || NF==3 {print;next;} NF>3{b=b"/../"; e=$NF; n-=length(b $NF); for (i=NF-1;i>3 && n>length(e)+1;i--) e=$i"/"e;} {print b e;}'\'')'

PS_INFO="${RED}\u@\h${RESET}:${YELLOW}${PS_PATH}${RESET}"

PS_GIT="${COLOR_GITBRANCH}\${PS_BRANCH}${RESET}"

PS_CTHULHU=""
#PS_CTHULHU="${GREEN}/|\\(${red}*${GREEN}>,,,<${red}*${GREEN})/|\\ ${RESET}\n"

PS_TIME="\[\033[\$((COLUMNS-10))G\]${RESET} [${YELLOW}\@${RESET}]"

export PS1="${PS_CTHULHU}${COLOR_DASHES}\${PS_FILL}\[\033[0G\]${PS_INFO} ${PS_GIT}${PS_TIME}\n${PS_PRE}${RESET}\$ "

set +x

