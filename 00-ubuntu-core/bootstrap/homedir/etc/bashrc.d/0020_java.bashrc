#!/usr/bin/env bash
scriptpath=$(dirname  ${BASH_SOURCE:?})
scriptname=$(basename ${BASH_SOURCE:?})

bashrc_notify "     ${color_loadmsg}LOAD${normal}: ${scriptname}"

#---------------------
# JAVA Settings
#---------------------
export JAVA_HOME=/usr/lib/jvm/java-11-openjdk
bashrc_notify "         :${cyan} Set ${yellow}JAVA_HOME${cyan} = ${yellow}${JAVA_HOME:?}${normal}"

export PATH=${PATH:?}:${JAVA_HOME:?}/bin



