#!/usr/bin/env bash
scriptpath=$(dirname  ${BASH_SOURCE:?})
scriptname=$(basename ${BASH_SOURCE:?})

bashrc_notify "     ${color_loadmsg}LOAD${normal}: ${scriptname}"

envvar_prepend_or_create PATH ${HOME}/bin

