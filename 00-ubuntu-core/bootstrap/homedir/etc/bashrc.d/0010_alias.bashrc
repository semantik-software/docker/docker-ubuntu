#!/usr/bin/env bash
set +x
scriptdir=$(dirname $BASH_SOURCE)
scriptname=$(basename $BASH_SOURCE)

bashrc_notify "     ${color_loadmsg}LOAD${nc}: ${scriptname}"


#------------------------------------------------------
# Alias configuration
#------------------------------------------------------
alias ll='ls -ltrhF --color=auto --group-directories-first'
alias ls='ls -1 -F --color=auto --group-directories-first'
alias lln='ls -lhF --color=auto --group-directories-first'
alias lsexec='find . -maxdepth 1 -perm -111 -type f'
alias h='history'
alias where='which'
alias csv2table='column -t -s","'
alias env='env | sort'




# reset verbosity
set +x

