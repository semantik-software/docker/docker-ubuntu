#!/usr/bin/env bash
scriptpath=$(dirname  ${BASH_SOURCE:?})
scriptname=$(basename ${BASH_SOURCE:?})

bashrc_notify "     ${color_loadmsg}LOAD${normal}: ${scriptname}"


#-----------------------
# Envvar Proxy Settings
#-----------------------

#export HTTP_PROXY="wwwproxy.acme.org:80"
#export HTTPS_PROXY="wwwproxy.acme.org:80"
#export FTP_PROXY="wwwproxy.acme.org:80"
#export FTPS_PROXY="wwwproxy.acme.org:80"
export NO_PROXY=".acme.org,localhost"


