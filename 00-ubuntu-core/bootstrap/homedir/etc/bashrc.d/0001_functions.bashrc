#!/usr/bin/env bash
scriptpath=$(dirname  ${BASH_SOURCE:?})
scriptname=$(basename ${BASH_SOURCE:?})

#---------------------

color_loadmsg=${green}
color_version=${green}
color_notify=${ice}
color_warning=${red}
color_envvar=${purple}
color_value=${red}



bashrc_notify() {
    printmsg=0
    if [ ! -n "$SSH_CLIENT" ] && [ ! -n "$SSH_TTY" ]; then
        printmsg=1
    fi
    if [[ "$printmsg" == "1" ]]; then
        scriptname=$(basename ${BASH_SOURCE:?})
        echo -e "$1"
    fi
}
bashrc_notify "     ${color_loadmsg}LOAD${normal}: ${scriptname}"



# get the top-level directory of the git repository
bashrc_git_root() {
    #local scriptpath=$(dirname  ${BASH_SOURCE:?})
    #local scriptname=$(basename ${BASH_SOURCE:?})
    local git_root=$(git rev-parse --show-toplevel)
    echo "${git_root:?}"
}



bashrc_git_bashrc_dir() {
    local git_root=$(bashrc_git_root)
    echo "${git_root}/etc/bashrc.d"
}


#============================================================
#
#  ALIASES AND FUNCTIONS
#
#  Arguably, some functions defined here are quite big.
#  If you want to make this file smaller, these functions can
#+ be converted into scripts and removed from here.
#
#============================================================

#-------------------------------------------------------------
# File & strings related functions:
#-------------------------------------------------------------

# Find a file with a pattern in name:
function rgrep() { grep -R -S -n --color=always "$1" * ; }

# Find a file with a pattern in name:
function ff() { find . -type f -iname '*'"$*"'*' -ls ; }

# Find a file with pattern $1 in name and Execute $2 on it:
function fe() { find . -type f -iname '*'"${1:-}"'*' \
-exec ${2:-file} {} \;  ; }

#  Find a pattern in a set of files and highlight them:
#+ (needs a recent version of egrep).
function fstr()
{
    OPTIND=1
    local mycase=""
    local usage="fstr: find string in files.
Usage: fstr [-i] \"pattern\" [\"filename pattern\"] "
    while getopts :it opt
    do
        case "$opt" in
           i) mycase="-i " ;;
           *) echo "$usage"; return ;;
        esac
    done
    shift $(( $OPTIND - 1 ))
    if [ "$#" -lt 1 ]; then
        echo "$usage"
        return;
    fi
    find . -type f -name "${2:-*}" -print0 | \
xargs -0 egrep --color=always -sn ${case} "$1" 2>&- | more

}


function swap()
{ # Swap 2 filenames around, if they exist (from Uzi's bashrc).
    local TMPFILE=tmp.$$

    [ $# -ne 2 ] && echo "swap: 2 arguments needed" && return 1
    [ ! -e $1 ] && echo "swap: $1 does not exist" && return 1
    [ ! -e $2 ] && echo "swap: $2 does not exist" && return 1

    mv "$1" $TMPFILE
    mv "$2" "$1"
    mv $TMPFILE "$2"
}

function extract()      # Handy Extract Program
{
    if [ -f $1 ] ; then
        case $1 in
            *.tar.bz2)   tar xvjf $1     ;;
            *.tar.gz)    tar xvzf $1     ;;
            *.bz2)       bunzip2 $1      ;;
            *.rar)       unrar x $1      ;;
            *.gz)        gunzip $1       ;;
            *.tar)       tar xvf $1      ;;
            *.tbz2)      tar xvjf $1     ;;
            *.tgz)       tar xvzf $1     ;;
            *.zip)       unzip $1        ;;
            *.Z)         uncompress $1   ;;
            *.7z)        7z x $1         ;;
            *)           echo "'$1' cannot be extracted via >extract<" ;;
        esac
    else
        echo "'$1' is not a valid file!"
    fi
}


# Creates an archive (*.tar.gz) from given directory.
function maketar() { tar cvzf "${1%%/}.tar.gz"  "${1%%/}/"; }

# Create a ZIP archive of a file or folder.
function makezip() { zip -T -r "${1%%/}.zip" "$1" ; }

# Make your directories and files access rights sane.
function sanitize() { chmod -R u=rwX,g=rX,o= "$@" ;}

#-------------------------------------------------------------
# Misc utilities:
#-------------------------------------------------------------

function repeat()       # Repeat n times command.
{
    local i max
    max=$1; shift;
    for ((i=1; i <= max ; i++)); do  # --> C-like syntax
        eval "$@";
    done
}


#-------------------------------------------------------------
# Timeout Utility (for Darwin only)
#-------------------------------------------------------------
if [[ "$PLATFORM" == "darwin" ]]; then

function timeout()
{
  perl -e 'alarm shift; exec @ARGV' "$@";
}

fi


#-------------------------------------------------------------
# Process/system related functions:
#-------------------------------------------------------------

function my_ps() { ps $@ -u $USER -o pid,%cpu,%mem,bsdtime,command ; }
function pp() { my_ps f | awk '!/awk/ && $0~var' var=${1:-".*"} ; }

function mydf()         # Pretty-print of 'df' output.
{                       # Inspired by 'dfc' utility.
    for fs ; do

        if [ ! -d $fs ]
        then
          echo -e $fs" :No such file or directory" ; continue
        fi

        local info=( $(command df -P $fs | awk 'END{ print $2,$3,$5 }') )
        local free=( $(command df -Pkh $fs | awk 'END{ print $4 }') )
        local nbstars=$(( 20 * ${info[1]} / ${info[0]} ))
        local out="["
        for ((j=0;j<20;j++)); do
            if [ ${j} -lt ${nbstars} ]; then
               out=$out"*"
            else
               out=$out"-"
            fi
        done
        out=${info[2]}" "$out"] ("$free" free on "$fs")"
        echo -e $out
    done
}


function my_ip() # Get IP adress on ethernet.
{
    MY_IP=$(/sbin/ifconfig eth0 | awk '/inet/ { print $2 } ' | sed -e s/addr://)
    echo ${MY_IP:-"Not connected"}
}


function ii()   # Get current host related info.
{
    echo -e "\nYou are logged on $HOST"
    echo -e "\nAdditionnal information: " ; uname -a
    echo -e "\nUsers logged on: " ; w -hi | cut -d " " -f1 | sort | uniq
    echo -e "\nCurrent date : " ; date
    echo -e "\nMachine stats : " ; uptime
    echo -e "\nMemory stats : " ; free
    echo -e "\nDiskspace : " ; mydf / $HOME
    echo -e "\nLocal IP Address :" ; my_ip
    #echo -e "\nOpen connections : "; netstat -an -p tcp;
    echo ""
}


function set_session_type()
{
    SESSION_TYPE=""
    if [ -n "$SSH_CLIENT" ] || [ -n "$SSH_TTY" ]; then
        SESSION_TYPE=remote/ssh
    # many other test cases should go here.
    else
        case $(ps -o comm= -p $PPID) in
            sshd|*/sshd) SESSION_TYPE=remote/ssh;;
        esac
    fi
    export SESSION_TYPE
}


function banner_welcome() {
    if hash toilet 2>/dev/null; then
        # toilet -w90 -f smblock --filter border:metal Welcome to `hostname`
        # toilet -w90 -f smblock --filter border:gay Welcome to `hostname`
        toilet -w90 -f future --filter border Welcome to `hostname`
    elif hash figlet 2>/dev/null; then
        figlet -w 90 -f threepoint Welcome to `hostname`
    else
        echo "============================================"
        echo " Welcome to" `hostname`
        echo "============================================"
    fi
}


# checked_module_load()
#   Conditionally loads a module from an environment var with a check against the
#   value of said envvar to ensure it's valid.  Load a default module if the given
#   envvar fails the check.
#
#     Param1: (str) REGEXP to check SEMS module name.
#     Param2: (str) Name of the envvar containing a module to load.
#     Param3: (str) Default module to load.
function checked_module_load() {
    echo "checked_module_load()"
    if [[ ! -n "${!2+1}" ]]; then
        echo "... ${2} is not set"
        echo "... Loading module: '${3}'"
        module load ${3}
    elif [[ "${!2}" =~ ${1} ]]; then
        echo "... ${2} matched ${!2}"
        echo "... This is a valid SEMS module of the format '${1}'."
        echo "... Loading module: '${2}'"
        module load ${!2}
    else
        echo "... ${2} did not match '${!2}'"
        echo "... This is NOT a valid SEMS module of the format '${1}'"
        echo "... Loading module: '${3}'"
        module load ${3}
    fi
}


# envvar_append_or_create
#  $1 = envvar name
#  $2 = string to append
function envvar_append_or_create() {
    # envvar $1 is not set
    if [[ ! -n "${!1+1}" ]]; then
        export ${1}="${2}"
    else
        export ${1}="${!1}:${2}"
    fi
}


# envvar_prepend_or_create
#  $1 = envvar name
#  $2 = string to prepend
function envvar_prepend_or_create() {
    # envvar $1 is not set
    if [[ ! -n "${!1+1}" ]]; then
        export ${1}="${2}"
    else
        export ${1}="${2}:${!1}"
    fi
}


# envvar_set_or_create
#  $1 = envvar name
#  $2 = string to prepend
function envvar_set_or_create() {
    export ${1}="${2}"
}


# param 1: Title
# param 2: Text
function notification() {
    if [ -f /usr/bin/osascript ]; then
        /usr/bin/osascript \
          -e "set theDialogText to \"${2}\n\" & (current date)" \
          -e 'set homeDir to path to home folder as string' \
          -e 'set failIco to homeDir & "etc:icons:Fail.icns"' \
          -e 'set passIco to homeDir & "etc:icons:Pass.icns"' \
          -e "display dialog  theDialogText buttons {\"Ok\"} default button \"Ok\" with title \"$1\" with icon file passIco"
    else
        echo "=====[ $1 ]=================================="
        echo "="
        echo "= $2"
        echo "="
        echo "= ($(date))"
        echo "="
    fi
}
export -f notification


# param 1: Title
# param 2: Text
function notification_pass() {
    if [ -f /usr/bin/osascript ]; then
        /usr/bin/osascript \
          -e "set theDialogText to \"${2} PASSED\n\" & (current date)" \
          -e 'set homeDir to path to home folder as string' \
          -e 'set failIco to homeDir & "etc:icons:Fail.icns"' \
          -e 'set passIco to homeDir & "etc:icons:Pass.icns"' \
          -e 'set boratHappy to homeDir & "etc:icons:BoratHappy.jpg"' \
          -e 'set klingonSuccess to homeDir & "etc:icons:klingon-success.jpg"' \
          -e "display dialog theDialogText buttons {\"Ok\"} default button \"Ok\" with title \"$1\" with icon file boratHappy"
    else
        echo "=====[ $1 ]=================================="
        echo "="
        echo "= $2 PASSED"
        echo "="
        echo "= ($(date))"
        echo "="
    fi
}
export -f notification_pass


# param 1: Title
# param 2: Text
function notification_fail() {
    if [ -f /usr/bin/osascript ]; then
        /usr/bin/osascript \
          -e "set theDialogText to \"${2} FAILED\n\" & (current date)" \
          -e 'set homeDir to path to home folder as string' \
          -e 'set failIco to homeDir & "etc:icons:Fail.icns"' \
          -e 'set passIco to homeDir & "etc:icons:Pass.icns"' \
          -e 'set klingonFailure to homeDir & "etc:icons:klingon-failure.jpg"' \
          -e 'set klingonSuccess to homeDir & "etc:icons:klingon-success.jpg"' \
          -e "display dialog  theDialogText buttons {\"Ok\"} default button \"Ok\" with title \"$1\" with icon file klingonSuccess"
    else
        echo "=====[ $1 ]=================================="
        echo "="
        echo "= $2 FAILED"
        echo "="
        echo "= ($(date))"
        echo "="
    fi
}
export -f notification_fail


# param 1: Title
# param 2: Text
function notice() {
    if [ -f /usr/bin/osascript ]; then
        /usr/bin/osascript \
          -e 'set sysIconPath to "/System/Library/CoreServices/CoreTypes.bundle/Contents/Resources/"' \
          -e 'set theIcon to sysIconPath & "FinderIcon.icns"' \
          -e "set theTitle to \"${1}\"" \
          -e "set theText to \"${2}\""  \
          -e "display dialog theText buttons {\"Ok\"} with title theTitle with icon {theIcon}"
    else
        echo "=====[ $1 ]=================================="
        echo "="
        echo "= $2"
        echo "="
        echo "= ($(date))"
        echo "="
    fi
}
export -f notice


function random_string_32() {
    #local random_32=$(date "+%s-%N" | sha256sum | base64 | head -c 32)
    local random_32=$(echo "${RANDOM}" | sha256sum | base64 | head -c 32)
    echo -e "${random_32}"
}

function string_exists_in_file() {
    local filename=${1:?}
    local string=${2:?}
    local exists=`cat ${filename} | grep ${string} | wc -l`
    echo ${exists}
}


# reset verbosity
set +x




