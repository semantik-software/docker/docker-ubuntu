#!/usr/bin/env bash

cmake_ver=3.26.4

bootstrap_options=(
    --parallel=4
    #--qt-gui
)

wget https://github.com/Kitware/CMake/releases/download/v${cmake_ver:?}/cmake-${cmake_ver:?}.tar.gz && \
    tar -zxvf cmake-${cmake_ver:?}.tar.gz && \
    cd cmake-${cmake_ver:?} && \
    ./bootstrap ${bootstrap_options[*]} && \
    make -j 4

err=$?

if [ $err -ne 0 ]; then
    echo -e "${red}ERROR - build failed${normal}"
    return $err
fi

echo -e "${green}Build succeeded${normal}"
echo -e ""
echo -e "Enter the following to install:"
echo -e "${magenta}cd cmake-${cmake_ver:?}${normal}"
echo -e "${magenta}sudo make install${normal}"




#    ./bootstrap --qt-gui --parallel=4 && \

#rm -rf cmake-${cmake_ver:?}
#rm cmake-${cmake_ver:?}.tar.gz
