# ubuntu-docker

A simple Dockerfile for creating a basic Ubuntu container with some helper scripts.




Building
========
Building images is done using the `run-build.sh` script,
which can take the following arguments:
- `--add-path-to-homedir=<path>` : Image copies the specifid `path`
  and its subdirectories into the `${HOME}` directory of the docker
  user account. This parameter can be provided 0 or more times.
- `--append-dockersnip=<docker_fragment_file>` : Appends the Docker
  fragment file specified to the end of the Dockerfile during image
  building. This parameter can be provided 0 or more times.

If there are multiple base images provided in `params.sh` the user
will be interactively asked to specify the version to build. This
query can be skipped by setting the value in an environment
variable: `DOCKERBUILD_IMAGE_VER`.  For example, to specify
Ubuntu 18.04 as the base image you can set the value to `18.04`
such as:
```bash
export DOCKERBUILD_IMAGE_VER=20.04
```


Useful Links
============
- Help with installing Java on Ubuntu 20.04 is [here][1].
- Ubuntu 20.04 LTS (Focal) Package search is [here][2].


[1]: https://www.digitalocean.com/community/tutorials/how-to-install-java-with-apt-on-ubuntu-20-04
[2]: https://packages.ubuntu.com/search?suite=focal&keywords=search
